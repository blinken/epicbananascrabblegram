extern crate rand;
extern crate nix;

use std::ascii::AsciiExt;
use std::char;
use std::io::{self,BufRead};
use nix::sys::signal;

// 1  point:  E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4
// 2  points: D ×4, G ×3
// 3  points: B ×2, C ×2, M ×2, P ×2
// 4  points: F ×2, H ×2, V ×2, W ×2, Y ×2
// 5  points: K ×1
// 8  points: J ×1, X ×1
// 10 points: Q ×1, Z ×1

fn rotate(word: &str) -> String {
  let scrabble = vec![1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10];
  let mut result = String::new();

  for c in word.to_ascii_lowercase().chars() {
    if c < 'a' || c > 'z' {
      result.push(c);
    } else {
      let new_index = ((c as u8) + 97 - scrabble[(c as usize) - 97]) % 26 + 97;
      //println!("{} {} {}", char, (char as u8), new_index as char);
      result.push(new_index as char);
    }
  }

  result
}

extern fn handle_sig(_:i32) {
  let facts: Vec<&'static str> = vec![
    "Rust is a general-purpose, multi-paradigm, compiled programming language sponsored by Mozilla Research.",
    "A large portion of Rust's current commits are from community members.",
    "The goal of Rust is to be a language suited to creating highly concurrent and highly safe systems.",
    "Rust does not use an automated garbage collection system like those used by Go, Java or .NET Framework.",
    "Rust's type system supports a mechanism similar to type classes, called 'traits', inspired directly by the Haskell language. This is a facility for ad-hoc polymorphism.",
    "Rust is designed to be memory safe, and it does not permit null pointers or dangling pointers.",
  ];

  if (rand::random::<u32>() % 4 == 0) {
    let mut rng = rand::thread_rng();
    let fact: &'static str = rand::sample(&mut rng, facts, 1).first().unwrap();
    println!("WHOA!! {}", fact);
  }
}

fn main() {
  let words: Vec<&'static str> = vec!["cake", "split", "smoothie", "muffins", "bread", "icecream", "fritter"];
  let hashes1: Vec<&'static str> = vec![
    "2510c39011c5be7041", // h
    "e358efa489f5806", // t
    "e358e", // t
    "83878c91171338902e0fe0fb", // p
    "853ae90f0351324bd73ea61", // :
    "6666cd76f969", // /
    "6666cd76f", // /
    "865c0c0b4ab0e063e5ca", // i
    "6f8f57715090da26324", // m
    "b2f5ff47", // g
    "7b774effe4a3", // u
    "4b43b0aee35624cd95b91", // r
    "5058f1af8388633f609ca", // .
    "4a8a08f09d37", // c
    "d95679752134a2d9eb61dbd7b91c4", // o
    "6f8", // m
    "6666cd76f96956469e7be39d", // /
    "44c29edb103a2872f519ad0c9a0fdaaa", // P
    "83878c91171338902e0fe0fb", // p
    "a87ff679a2f3e71d9181", // 4
    "7b8b965ad4bca0e41ab51de7b31363a1", // n
    "e4da3b7fbbce2345d7772b0674a318d5", // 5
    "c9f0f895fb98a", // 8
    "5dbc98dcc983a707", // S
  ];

  let hashes2: Vec<&'static str> = vec![
    "82423e3a695e91", // h
    "2f10dd7316b65649e", // t
    "fa489f58062f10dd7316b65649e", // t
    "97a8c47a", // p
    "5e6487517", // :
    "56469e7be39d750cc7d9", // /
    "96956469e7be39d750cc7d9", // /
    "a3387c1a8741", // i
    "53988d9a1501b", // m
    "436671b6e533d8dc3614845d", // g
    "49c6dd82ad4f4f21d34c", // u
    "0189b3dc231", // r
    "db75a75dc9d", // .
    "b73795649038408b5f33", // c
    "bcc", // o
    "f57715090da2632453988d9a1501b", // m
    "750cc7d9", // /
    "", // P
    "97a8c47a", // p
    "a67b7542122c", // 4
    "", // n
    "", // 5
    "b9159f51fd0297e236d", // 8
    "28bd082d1a47546e", // S
  ];
    
  unsafe {
    let sig_action = signal::SigAction::new(signal::SigHandler::Handler(handle_sig), signal::SaFlags::empty(), signal::SigSet::empty());
    signal::sigaction(signal::SIGWINCH, &sig_action);
  }

  let mut rng = rand::thread_rng();
  let word: &'static str = rand::sample(&mut rng, words, 1).first().unwrap();
  println!("{}", rotate(word));

  let mut buffer = String::new();
  let stdin = io::stdin();
  stdin.lock().read_line(&mut buffer).unwrap();

  let mut mult = 1;
  if buffer == "sw0rdf1sh\n" {
    mult = 0;
  }

  let mut x: u32 = 0;
  for n in 0..hashes1.len() {
    for x in 0..((rand::random::<u32>() % 500000000) * mult) { rotate("oh hai, hackers!"); }
    println!("{}{}", rotate(hashes1[n]), rotate(hashes2[n]));
  }
}
